 
# **CAPE**


## Comparison of avatar models and patients expression profiles

The system merges two cohorts, one of patients and one of models, to analyse clustering and gene modules using Non-negative matrix factorization prior to data correction.

The script includes instructions for basic use as well as tools for improving the process.

> Author: carlos.company.bioinf@gmail.com

### Basic usages 

#### [ Step 0 > Load Variables ] 

<details><summary>Click to expand</summary>

```
# Variables
library(tidyverse) 
source('capeo/R/cape_script.R') 

# Input data-sets 
glioma_genes = read.table('gig_wang2017.txt', stringsAsFactors=F)[,1] 
housekeeping_genes = read.table('Housekeeping_GenesHuman.tsv', stringsAsFactors=F,sep ='\t')[,2]
model_set = readRDS('gsa_model_exp.rds') # GSA model expression profile dataset 
patient_set = readRDS('TCGA_GBM_exp.rds') # Patient expression profiles dataset 

# Input variables
args = commandArgs(trailingOnly=TRUE) 
u_ruv = args[1] %>% as.integer # To define the number of K-factor for batch correction 
u_clust_nmf =  args[2] %>% as.character # Definition of NMF method ( based on http://renozao.github.io/NMF/ ) 
u_neg_genes = args[3] %>% as.integer # To define the number of neg. control genes ( if necessary ) 
u_name_analysis = args[4] %>% as.character # Name data-sets 
```  
</details>

### [ Step 1 > Combine the datasets and annotate the matrix ]

<details><summary>Click to expand</summary>

```
# Generate a list with the data 
data_set_list <- list(
    'REAL' = list('TCGA' = patient_set),
    'MODEL' = list('GSAB1' = model_set$gsa_first, 
                   'GSAB2' = model_set$gsa_first )
    ) 

# Combine matrix 
combined_list = combineMatrix( 
   real_mat = data_set_list$REAL, 
   model_mat = data_set_list$MODEL, 
   is_list = TRUE ) 
```

</details>

### [ Step 2 > Normalize the dataset ]

<details><summary>Click to expand</summary>

```
se_dat  <- normalizeMatrix(
  input_list = combined_list, 
  min_count=0, 
  downsample = NULL) 
```
</details>

### [ Step 3 > Data correction ]
<details><summary>Click to expand</summary>

```
se_dat2 <- nmfBatchCorrect( 
      se_obj = se_dat, 
      genes_KEEP = glioma_genes,  
      var_num = u_ruv, 
      top_neg_gene = u_neg_genes
      ) 
```
</details>

### [ Step 4 > Data evaluation using NMF ]

<details><summary>Click to expand</summary>

```
se_dat3 <- clustNMF(se_obj = se_dat2,  
    nmf_runs = 10,  
    nmf_factors = 2:7, 
    name_clust = 'comb', 
    nmf_method = u_clust_nmf,  
    min_count=0,   
    nmf_method_genes= u_clust_nmf 
  ) 
```
</details>


### [ Step 5 > Custom algorithms ]

<details><summary>Click to expand</summary>

```
source('extra_functions.R')
```
</details>
